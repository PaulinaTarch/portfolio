# About me

Hello world!

My name is Paulina. I'm glad that you decided to visit my portfolio :)

I create this place to gather and share my achievements, skills in testing software.

I’m a self-learner manual tester. 

Currently, I work as a customer service specialist at an online shop. 

I still develop my testing skills so if you have any advice or just question please send me a message on my Linkedin profile.  I will appreciate it :).

My Linkedin profile:
- www.linkedin.com/in/paulinatarchała

# Projects

1. [SAFESAFE](http://cherry-it.pl/podsumowanie-naboru-ad-hoc-safesafe/) (after time changed name on ProteGO Safe) 
2. [WHO2020](http://cherry-it.pl/who-podsumowanie-naboru/) 

Mobile apps were created due to COVID-19. The applications were tested by a team of beginners testers like me :).
Both projects were run by experienced software tester [Cherry-IT Anna Czyrko](http://cherry-it.pl/). 
In projects, I used Mantis, Trello, and JIRA to reported bugs.
My issues from SafeSafe project you find below (Documentation).
Unfortunately, I can't publish my issues of WHO2020 project cause of confidential data.

# Tools and technologies
- Selenium IDE
- Mantis Bug Tracker
- Trello
- JIRA
- SQL basics
- Greenshot
- Xmind
- Bug Magnet
- SLACK
- Postman basics
- WAVE Web Accessibility Evaluation Tool
- DevTools

# Documentation
1. Project Safe Safe [PL]
   - [My reported bugs](https://drive.google.com/file/d/1Sa3xtu8wQnPtpskbzNo75t-ZRvHqNs6M/view?usp=sharing)  
2. My invidual project - testing site http://automationpractice.com [PL]
   - [Test cases](https://drive.google.com/file/d/1dCT7gRAESxcTPMtr9Bn8MYKNyHqkuSHm/view?usp=sharing)
   - [Automated SELENIUM IDE tests](https://drive.google.com/file/d/1wTXBKl-ESN0qK7_5AUkLaKyscMCWOpA0/view?usp=sharing)
   - [Example Mind map](https://drive.google.com/file/d/1HG9aQdCdLEJ-t7xk9TiUE3dBWf4nQg4X/view?usp=sharing)
3. Bugs report - testing site https://www.sklepogrodniczy.pl [PL]
   - [My reported bugs](https://drive.google.com/file/d/12XP0GPdSmB6esrlSWlmvlZIyyimtbDp1/view?usp=sharing)
4. Testing REST API [EN]
   - [Postman Collection](https://drive.google.com/file/d/18oppmQpQvXpmC2S8-2YX7ae6DxRNZjdN/view?usp=sharing)


# Webinars
1. Conference [NO BS ABOUT...TESTING 2020](https://nobullshitabout.com/testing-2020), 08.10.2020
    - [Certificate](https://drive.google.com/file/d/1bNWeBchtFGTwFi7p53J7J0n7OJhcIWwI/view?usp=sharing)
2. Conference [beIT Online - ścieżka beUXDesigner](https://zarzadzanieit.com/Content/Beit/2020/), 13.10.2020
3. LogCat Mobile QA Meetup Online, 'Creating and using Personas in mobile projects' Maciej Jaskulski, 15.10.2020
4. QA Tester Online Meetup #2., 'Specification-based testing techniques' Olga Kamińska-Nowicka, 20.10.2020

# Courses
1. 'Introduction to software testing', Mateusz Kruszyk (Udemy.com)
   - [Certificate](https://www.udemy.com/Certificate/UC-69e0ff3f-0a89-4eef-a814-cc52aa7bc61e/)
2. 'MySQL creating and managing databases', Krzysztof Raczynski (Udemy.com)
   - [Certificate](https://www.udemy.com/Certificate/UC-63134442-4153-429c-8093-3de030868a9e/)
3. 'Postman - REST API Testing', Tester oprogramowania (Udemy.com) 
   - [Certificate](https://www.udemy.com/Certificate/UC-83ed0844-baa1-4928-81f4-2523ca44f7dc/)
4. 'HTML5 & CSS3 Workshops', Foundation CODE:ME (Online) 
5. 'Selenium Python course from scratch', Tester oprogramowania (Udemy.com) - in progress

# Books
- Jadczyk K.: Pasja Testowania.Książka dla każdego, kto chce zostać testerem. 2019
- Smilgin R.: Zawód tester. Od decyzji do zdobycia doświadczenia. 2018
- Szafraniec W.;Gabor D.: ABC Testowania oprogramowania. 2020
- Stowarzyszenie Jakości Systemów Informatycznych (SJSI).: Certyfikowany tester. Sylabus poziomu podstawowego ISTQB., Wersja 2018 V 3.1. 2018
- Krug S.: Nie każ mi myśleć! O życiowym podejściu do funkcjonalności stron internetowych. Wydanie III. 2014 – in progress
- A.Roman, L.Stapp.: Certyfikowany tester ISTQB Poziom podstawowy. 2020 - in progress

# Recommended blogs
- https://jaktestowac.pl/
- https://www.wyszkolewas.com.pl/
- https://remigiuszbednarczyk.pl/
- https://testuj.pl/
- https://testerzy.pl/
- https://www.toniebug.pl/
- http://cherry-it.pl/


