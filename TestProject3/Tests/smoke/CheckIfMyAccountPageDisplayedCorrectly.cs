﻿using NUnit.Framework;
using TestProject3.Framework.Pages;

namespace TestProject3.Tests.smoke
{
    public class CheckIfMyAccountPageDisplayedCorrectlyClass : MyAccountPage
    {
        [Test]
        public void CheckIfMyAccountPageDisplayedCorrectly()
        {
            GoToMyAccountPage();
            CheckIfMyAccountPagIsBeingDisplayed();
        }
    }
}
