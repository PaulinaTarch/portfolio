﻿using NUnit.Framework;
using TestProject3.Framework.Pages;

namespace TestProject3.Tests.smoke
{
    public class CheckIfShopPageDisplayedCorrectlyClass : ShopPage
    {
        [Test]
        public void CheckIfShopPageDisplayedCorrectly()
        {
            GoToShopPage();
            CheckIfShopPageIsBeingDisplayed();
        }
    }
}
