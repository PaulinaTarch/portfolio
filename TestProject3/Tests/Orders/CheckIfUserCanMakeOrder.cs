﻿using NUnit.Framework;
using TestProject3.Framework.Pages;

namespace TestProject3.Tests.Orders
{
    public class CheckIfUserCanMakeOrderClass : ShopPage
    {
        [Test]
        public void CheckIfUserCanMakeOrder()
        {
            GoToShopPage();
            CheckIfShopPageIsBeingDisplayed();
            ClickYogaAndPilatesProductCategory();
            AddProductToCart(61);
            GoToCart();
            GoToPayment();
            FillOrderForm("Jan", "Kowalski", "Mickiewicza 6", "33-100", "Tarnów", "766666666", "test@test.pl", "4242424242424242", "09 / 30", "123");
            AddAdditionalInformation();
            CheckTermsCheckbox();
            PlaceOrder();
            CheckIfOrderTookPlaced("Jan", "Kowalski");
;        }
    }
}
