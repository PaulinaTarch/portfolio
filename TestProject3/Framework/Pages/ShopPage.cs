﻿using OpenQA.Selenium;
using NUnit.Framework.Legacy;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace TestProject3.Framework.Pages
{
    public class ShopPage : Settings
    {
        public readonly static string shopPage = "//div//a[contains(text(),'Sklep')]";
        public readonly static string cartPage = "//div[@class='primary-navigation']//a[contains(text(),'Koszyk')]";

        public readonly static string yogaAndPilatesCategory = "//a[@aria-label='Przejdź do kategorii produktu Yoga i pilates']";

        public readonly static string firstNameInput = "billing_first_name";
        public readonly static string lastNameInput = "billing_last_name";
        public readonly static string addressInput = "billing_address_1";
        public readonly static string postcodeInput = "billing_postcode";
        public readonly static string cityInput = "billing_city";
        public readonly static string phoneInput = "billing_phone";
        public readonly static string emailInput = "billing_email";
        public readonly static string cardNumberInput = "//input[@name='cardnumber']";
        public readonly static string expireDateInput = "//input[@name='exp-date']";
        public readonly static string numerCVCInput = "//iframe[@title='Bezpieczne pole wprowadzania CVC']";
        public readonly static string additionalInformationInput = "order_comments";

        public readonly static string termsCheckbox = "//input[@name='terms']";

        public readonly static string placeOrderButton = "//button[@id='place_order']";
        public readonly static string paymentButton = "//div[contains(@class, 'wc-proceed-to-checkout')]";
        public readonly static string shopPageButton = "//li[@id=\"menu-item-198\"]//a[contains(text(),'Sklep')]";


        public static void GoToShopPage()
        {
            driver.FindElement(By.XPath(shopPage)).Click();
            Console.WriteLine("Clicked 'Shop' button.");
        }

        public static void GoToCart()
        {
            Thread.Sleep(3000);
            driver.FindElement(By.XPath(cartPage)).Click();
            Console.WriteLine("Clicked 'Cart' button.");
        }

        public static void GoToPayment()
        {
            Thread.Sleep(3000);
            driver.FindElement(By.XPath(paymentButton)).Click();
        }

        public static void FillOrderForm(string firstName, string lastName, string address, string postcode,
                                         string city, string phoneNumber, string email, string cardNumber, string expireDate, string cardCVC)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));

            Thread.Sleep(5000);
            driver.SwitchTo().Frame(2);
            driver.FindElement(By.XPath(cardNumberInput)).SendKeys(cardNumber);
            driver.FindElement(By.XPath(expireDateInput)).SendKeys(expireDate);
            driver.SwitchTo().DefaultContent();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(numerCVCInput)));
            driver.FindElement(By.XPath(numerCVCInput)).Click();
            driver.FindElement(By.XPath(numerCVCInput)).SendKeys(cardCVC);
            driver.FindElement(By.Id(firstNameInput)).SendKeys(firstName);
            driver.FindElement(By.Id(lastNameInput)).SendKeys(lastName);
            driver.FindElement(By.Id(addressInput)).SendKeys(address);
            driver.FindElement(By.Id(postcodeInput)).SendKeys(postcode);
            driver.FindElement(By.Id(cityInput)).SendKeys(city);
            driver.FindElement(By.Id(phoneInput)).SendKeys(phoneNumber);
            driver.FindElement(By.Id(emailInput)).SendKeys(email);
        }

        public static void CheckIfShopPageIsBeingDisplayed()
        {
            try
            {
                ClassicAssert.IsTrue(driver.FindElement(By.XPath(shopPageButton)).Displayed);
                Console.WriteLine("Shop is being displayed!");
            }
            catch (Exception)
            {
                Console.WriteLine("Sorry! ShopPage is not being displayed");
                TakeScreenshot();
                throw;
            }
        }

        public static void ClickYogaAndPilatesProductCategory()
        {
            driver.FindElement(By.XPath(yogaAndPilatesCategory)).Click();
        }

        public static void AddProductToCart(int productId)

        {
            Thread.Sleep(3000);
            driver.FindElement(By.XPath($"//ul[@class='products columns-3']//a[@href='?add-to-cart={productId}']")).Click();
        }

        public static void CheckTermsCheckbox()
        {
            Thread.Sleep(3000);
            driver.FindElement(By.XPath("//input[@name='terms']")).Click();
        }

        public static void PlaceOrder()
        {
            Thread.Sleep(3000);
            driver.FindElement(By.Name("woocommerce_checkout_place_order")).Click();
        }

        public static void CheckIfOrderTookPlaced(string firstName, string lastName)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//h1[contains(text(),'Zamówienie otrzymane')]")));

            try
            {
                ClassicAssert.IsTrue(driver.FindElement(By.XPath($"//section[@class='woocommerce-customer-details']//address[contains(text(),'{firstName} {lastName}')]")).Displayed);
                Console.WriteLine("Order was received!");
            }
            catch (Exception)
            {
                Console.WriteLine("Sorry! Order was not received");
                TakeScreenshot();
                throw;
            }
        }

        public static void AddAdditionalInformation()
        {
            Random res = new Random();

            String str = "aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ";
            int size = 15;

            String information = "";

            for (int i = 0; i < size; i++)
            {
                int x = res.Next(26);
                information = information + str[x];
            }

            Thread.Sleep(3000);
            driver.FindElement(By.Id(additionalInformationInput)).Click();
            driver.FindElement(By.Id(additionalInformationInput)).SendKeys(information);
        }
    }
}