﻿using OpenQA.Selenium;
using NUnit.Framework.Legacy;

namespace TestProject3.Framework.Pages
{
    public class MyAccountPage : Settings
    {
        public readonly static string myAccountPage = "//header//h1[contains(text(),'Moje konto')]";

        public readonly static string myAccountPageButton = "//div[@class='primary-navigation']//a[contains(text(),'Moje konto')]";

        public static void GoToMyAccountPage()
        {
            driver.FindElement(By.XPath(myAccountPageButton)).Click();
            Console.WriteLine("Clicked 'my account' button");
        }

        public static void CheckIfMyAccountPagIsBeingDisplayed()
        {
            try
            {
                ClassicAssert.IsTrue(driver.FindElement(By.XPath(myAccountPage)).Displayed);
                Console.WriteLine("Account is being displayed!");
            }
            catch (Exception)
            {
                Console.WriteLine("Sorry! Account is not being displayed");
                TakeScreenshot();
                throw;
            }
        }
    }
}