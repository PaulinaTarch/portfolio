﻿using OpenQA.Selenium;
using NUnit.Framework.Legacy;

namespace TestProject3.Framework.Pages
{
    public class WishPage : Settings
    {
        public readonly static string wishlistPage = "//header//h1[contains(text(),'Lista życzeń')]";
        public readonly static string wishlistButton = "//div[@class='primary-navigation']//a[contains(text(),'Lista życzeń')]";

        public static void CheckIfMyAccountPagIsBeingDisplayed()
        {
            try
            {
                ClassicAssert.IsTrue(driver.FindElement(By.XPath(wishlistPage)).Displayed);
                Console.WriteLine("Wishpage is being displayed!");
            }
            catch (Exception)
            {
                Console.WriteLine("Sorry! Wishpage is not being displayed");
            }
        }
    }
}