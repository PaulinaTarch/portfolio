﻿using NUnit.Framework.Legacy;
using OpenQA.Selenium;

namespace TestProject3.Framework.Pages
{
    public class HomePage : Settings
    {
        public readonly static string mainTitle = "//header//h1[contains(text(),'Wybierz podróż dla siebie!')]";

        public static void CheckIfMainTitlePageIsBeingDisplayed()
        {
            try
            {
                ClassicAssert.IsTrue(driver.FindElement(By.XPath(mainTitle)).Displayed);
                Console.WriteLine("Main title is being displayed!");
            }
            catch 
            {
                Console.WriteLine("Sorry! Main title is not being displayed");
            }
        }
    }
}
