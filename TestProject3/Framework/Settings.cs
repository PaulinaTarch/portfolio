﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace TestProject3.Framework
{
    public class Settings 
    {
       public static IWebDriver driver { get; set; }

        public static string url = "https://fakestore.testelka.pl/";
                
        [SetUp]
            public static void SetUp()
            {
            ChromeOptions options = new ChromeOptions();
            /* options.AddArguments(new List<string>()
            {
             "--silent-launch",
             "--no-startup-window",
             "no-sandbox",
             "headless"
            });*/
            driver = new ChromeDriver(options);
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            Console.WriteLine("Page loaded successfully");
        }

        [TearDown]
        public void TearDown()
        {
            driver.Close();
        }

        public static void TakeScreenshot()
        {
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
            ss.SaveAsFile(@"D:\Screenshot\test.png");
        }
    }
}
